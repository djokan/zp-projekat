package projekat;

import java.awt.*;
import java.awt.event.*;
import java.math.BigInteger;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.security.*;
import java.text.SimpleDateFormat;
import java.util.Date;
public class GenerisiParKljuceva extends JDialog{
	public Program owner;
	private JTextField naziv,velicina,email,cn,ou,o,l,st,c;
	
	public GenerisiParKljuceva(Program owner) {
		super(owner,"Генериши пар кључева",true);
		this.owner=owner;
		setLayout(new GridLayout(10,1));
		JPanel p1= new JPanel();
		p1.setLayout(new GridLayout(1,2));
		JPanel p11= new JPanel();
		JLabel nazivlabel= new JLabel("Назив кључа:");
		p11.add(nazivlabel);
		p1.add(p11);
		JPanel p12= new JPanel();
		 naziv=new JTextField();
		naziv.setPreferredSize(new Dimension(100, 20));
		p12.add(naziv);
		p1.add(p12);
		add(p1);
		
		
		JPanel p2= new JPanel();
		p2.setLayout(new GridLayout(1,2));
		JPanel p21= new JPanel();
		JLabel velicinalabel= new JLabel("Величина кључа (у битовима):");
		 
		p21.add(velicinalabel);
		p2.add(p21);
		JPanel p22= new JPanel();
		 velicina=new JTextField();
		velicina.setPreferredSize(new Dimension(100, 20));
		p22.add(velicina);
		p2.add(p22);
		add(p2);
		
		JPanel p3= new JPanel();
		p3.setLayout(new GridLayout(1,2));
		JPanel p31= new JPanel();
		JLabel emaillabel= new JLabel("Мејл (E):");
		 
		p31.add(emaillabel);
		p3.add(p31);
		JPanel p32= new JPanel();
		 email=new JTextField();
		email.setPreferredSize(new Dimension(100, 20));
		p32.add(email);
		p3.add(p32);
		add(p3);
		
		
		JPanel p4= new JPanel();
		p4.setLayout(new GridLayout(1,2));
		JPanel p41= new JPanel();
		JLabel cnlabel= new JLabel("Уобичајено име (CN):");
		 
		p41.add(cnlabel);
		p4.add(p41);
		JPanel p42= new JPanel();
		 cn=new JTextField();
		cn.setPreferredSize(new Dimension(100, 20));
		p42.add(cn);
		p4.add(p42);
		add(p4);
		
		JPanel p5= new JPanel();
		p5.setLayout(new GridLayout(1,2));
		JPanel p51= new JPanel();
		JLabel oulabel= new JLabel("Организациона јединица (OU):");
		 
		p51.add(oulabel);
		p5.add(p51);
		JPanel p52= new JPanel();
		 ou=new JTextField();
		ou.setPreferredSize(new Dimension(100, 20));
		p52.add(ou);
		p5.add(p52);
		add(p5);
		
		JPanel p6= new JPanel();
		p6.setLayout(new GridLayout(1,2));
		JPanel p61= new JPanel();
		JLabel olabel= new JLabel("Организацја (O):");
		 
		p61.add(olabel);
		p6.add(p61);
		JPanel p62= new JPanel();
		 o=new JTextField();
		o.setPreferredSize(new Dimension(100, 20));
		p62.add(o);
		p6.add(p62);
		add(p6);
		
		JPanel p7= new JPanel();
		p7.setLayout(new GridLayout(1,2));
		JPanel p71= new JPanel();
		JLabel llabel= new JLabel("Локалитет (L):");
		 
		p71.add(llabel);
		p7.add(p71);
		JPanel p72= new JPanel();
		 l=new JTextField();
		l.setPreferredSize(new Dimension(100, 20));
		p72.add(l);
		p7.add(p72);
		add(p7);
		
		JPanel p8= new JPanel();
		p8.setLayout(new GridLayout(1,2));
		JPanel p81= new JPanel();
		JLabel stlabel= new JLabel("Држава (ST):");
		 
		p81.add(stlabel);
		p8.add(p81);
		JPanel p82= new JPanel();
		 st=new JTextField();
		st.setPreferredSize(new Dimension(100, 20));
		p82.add(st);
		p8.add(p82);
		add(p8);
		
		JPanel p9= new JPanel();
		p9.setLayout(new GridLayout(1,2));
		JPanel p91= new JPanel();
		JLabel clabel= new JLabel("Земља (C):");
		 
		p91.add(clabel);
		p9.add(p91);
		JPanel p92= new JPanel();
		 c=new JTextField();
		c.setPreferredSize(new Dimension(100, 20));
		p92.add(c);
		p9.add(p92);
		add(p9);
		
		
		
		
		
		JPanel p10= new JPanel();
		JButton pravi = new JButton("Генериши");
		pravi.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	try{
		    		if ( Integer.parseInt(velicina.getText())<1024)
		    		{
		    			new ErrorDialog(GenerisiParKljuceva.this,"Veličina veća ili jednaka od 1024");
		    		}
		    		else
		    		{
		    			try
		    			{
		    				KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
		    				keyGen.initialize(Integer.parseInt(velicina.getText()), new SecureRandom());
		    				KeyPair pair= keyGen.genKeyPair();
		    				
		    				
		    				ParKljuceva par= new ParKljuceva(pair, naziv.getText(), email.getText(), cn.getText(), ou.getText(), o.getText(), l.getText(), st.getText(), c.getText());
		    				
		    				GenerisiParKljuceva.this.owner.add(par);
		    				GenerisiParKljuceva.this.dispose();
		    			}
		    			catch(Exception e2)
		    			{
				    		new ErrorDialog(GenerisiParKljuceva.this,"Грешка у генерисању пара кључева, проверите да ли је лепо унето све");
		    				
		    			}
		    	   
		    	   
		    		}
		    	}
		    	catch (Exception e2)
		    	{
		    		new ErrorDialog(GenerisiParKljuceva.this,"Veličina mora biti broj!");
		    		
		    	}
		    	
		    	
		    	
		    }
		});
		p10.add(pravi);
		add(p10);
		
		
		addWindowListener(
				new WindowAdapter(){
					public void windowClosing(WindowEvent a){dispose();}
				}
				);
		
		setResizable(false);
		setSize(500,340);
		Utilities.centerScreen(this);
		setVisible(true);
	}

}
