package projekat;


import java.math.BigInteger;
import java.security.*;

import java.util.Date;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.KeyUsage;




public class ParKljuceva {
	private KeyPair keys;
	private String imekljuca,email,iantype,ianvalue,ku;
	private Date vremeod,vremedo;
	private BigInteger serial;
	private int pathlen,keyusageint;
	private String cn,ou,o,l,st,c;
	private boolean cA,ianenabled,kuenabled,bcenabled,bcc,ianc,kuc;
	
	public Date getVremeod() {
		return vremeod;
	}
	public Date getVremedo() {
		return vremedo;
	}
	public BigInteger getSerial() {
		return serial;
	}
	public int getKeyusageint() {
		return keyusageint;
	}
	public X500Name getX500Name(){
		return new X500Name("CN="+ getCn() + ", OU="+getOu() + ", O=" + getO() + ", L="+getL()+ ", ST="+getSt()+ ", C="+getC()+ ", E="+getEmail());
		
	}
	
	public boolean isIanenabled() {
		return ianenabled;
	}
	public boolean isKuenabled() {
		return kuenabled;
	}
	public boolean isBcenabled() {
		return bcenabled;
	}
	public String getIantype() {
		return iantype;
	}
	public String getIanvalue() {
		return ianvalue;
	}
	public String getKu() {
		return ku;
	}
	public boolean iscA() {
		return cA;
	}
	public int getPathlen() {
		return pathlen;
	}
	public KeyPair getKeys() {
		return keys;
	}
	public String getImekljuca() {
		return imekljuca;
	}
	public String getEmail() {
		return email;
	}
	public String getCn() {
		return cn;
	}
	public String getOu() {
		return ou;
	}
	public String getO() {
		return o;
	}
	public String getL() {
		return l;
	}
	public String getSt() {
		return st;
	}
	public String getC() {
		return c;
	}
	
	public boolean isBcc() {
		return bcc;
	}
	public boolean isIanc() {
		return ianc;
	}
	public boolean isKuc() {
		return kuc;
	}
	public void setBC(boolean c,boolean cA,int pathlen)
	{
		bcenabled=true;
		this.cA=cA;
		this.pathlen=pathlen;
		bcc=c;
	}
	public void setIAN(boolean c,String type,String value)
	{
		ianenabled=true;
		iantype=type;
		ianvalue=value;
		ianc=c;
	}
	public void setKU(boolean c,boolean k1,boolean k2,boolean k3,boolean k4,boolean k5,boolean k6,boolean k7,boolean k8,boolean k9)
	{
		kuenabled=true;
		kuc=c;
		ku="";
		keyusageint=0;
		if (k1)
		{
			ku+="Digital Signature,";
			keyusageint|=KeyUsage.digitalSignature;
		}
		if (k2)
		{
			ku+="Non Repuditarion,";
			keyusageint|=KeyUsage.nonRepudiation;
		}
		if (k3)
		{
			ku+="Key Enchiperment,";
			keyusageint|=KeyUsage.keyEncipherment;
		}
		if (k4)
		{
			ku+="Data Enchiperment,";
			keyusageint|=KeyUsage.dataEncipherment;
		}
		if (k5)
		{
			ku+="Key Agreement,";
			keyusageint|=KeyUsage.keyAgreement;
		}
		if (k6)
		{
			ku+="Cerificate Sign,";
			keyusageint|=KeyUsage.keyCertSign;
		}
		if (k7)
		{
			ku+="cRL Sign,";
			keyusageint|=KeyUsage.cRLSign;
		}
		if (k8)
		{
			ku+="Encipher Only,";
			keyusageint|=KeyUsage.encipherOnly;
		}
		if (k9)
		{
			ku+="Decipher Only,";
			keyusageint|=KeyUsage.decipherOnly;
		}
		if (ku.endsWith(","))
		{
			ku=ku.substring(0, ku.length()-1);
		}
	}
	public ParKljuceva()
	{
		ianenabled=false;
		bcenabled=false;
		kuenabled=false;
	}
	public ParKljuceva(KeyPair k,String s1, String email1, String cn1,String ou1,String o1,String l1,String st1,String c1)
	{
		ianenabled=false;
		bcenabled=false;
		kuenabled=false;
		keys=k;
		imekljuca=s1;
		email=email1;
		cn=cn1;
		ou=ou1;
		o=o1;
		l=l1;
		st=st1;
		c=c1;
	}
	public void setBasicParameters(Date poc, Date kraj, BigInteger ser) {
		vremeod=poc;
		vremedo=kraj;
		serial=ser;
		
	}
	
	
}
