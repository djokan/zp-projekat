package projekat;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;

import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.util.encoders.Base64;

public class CЕRParaKljuceva extends JDialog{
	private JCheckBox bcc,kuc,ianc,bcenable,catrue,ianenable,kuenable,ku1,ku2,ku3,ku4,ku5,ku6,ku7,ku8,ku9;
	private JTextField datum1,datum2,serial,ianvalue,bcpathlen;
	private JLabel chooselabel,choosecsrlabel;
	JComboBox<String> ianlist;
	private String fajl,csr;
	private Program owner;
	public CЕRParaKljuceva(JFrame owner) {
		super(owner,"Извоз пара кључева",true);
		setLayout(new GridLayout(12,1));
		fajl= "podrazumevano.cer";
		csr="podrazumevano.csr";
		this.owner= (Program) owner;
		addWindowListener(
				new WindowAdapter()
				{
					public void windowClosing(WindowEvent a)
					{
						dispose();
					}
				}
				);
		
		JPanel p1= new JPanel();
		p1.setLayout(new GridLayout(1,2));
		JPanel p11= new JPanel();
		choosecsrlabel= new JLabel("podrazumevano.csr");
		p11.add(choosecsrlabel);
		p1.add(p11);
		JPanel p12= new JPanel();
		
		
JButton choose = new JButton("Отвори CSR...");
		
		choose.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	JFileChooser fileChooser = new JFileChooser();
		        fileChooser.setAcceptAllFileFilterUsed(false);
		    	fileChooser.setFileFilter(new FileFilter() {
		    		 
		    	    public String getDescription() {
		    	        return "Certificate Signing Request (*.csr)";
		    	    }
		    	 
		    	    public boolean accept(File f) {
		    	        if (f.isDirectory()) {
		    	            return true;
		    	        } else {
		    	            return f.getName().toLowerCase().endsWith(".csr");
		    	        }
		    	    }
		    	});
		    	if (fileChooser.showOpenDialog(CЕRParaKljuceva.this) == JFileChooser.APPROVE_OPTION) 
		    	{
		    		String s= fileChooser.getSelectedFile().getAbsolutePath()+".csr";
		    		s=Utilities.SrediImeFajla(s, ".csr");
		    		csr=s;
		    		if (s.length()>15)
		    		s= "..."+s.substring(s.length()-15, s.length());
		    		CЕRParaKljuceva.this.choosecsrlabel.setText(s);
		    		try {
						new CSRInfoDialog(CЕRParaKljuceva.this,csr);
					} catch (Exception e1) {
						
						e1.printStackTrace();
					}
		    	}
		    }
		});
		
		
		p12.add(choose);
		p1.add(p12);
		add(p1);
		
		JPanel p2= new JPanel();
		p2.setLayout(new GridLayout(1,2));
		JPanel p21= new JPanel();
		p21.setLayout(new FlowLayout());
		chooselabel= new JLabel("podrazumevano.cer");
		p21.add(chooselabel);
		p2.add(p21);
		JPanel p22 = new JPanel();
		JButton choose2 = new JButton("Сачувај као...");
		
		choose2.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	JFileChooser fileChooser = new JFileChooser();
		        fileChooser.setAcceptAllFileFilterUsed(false);
		    	fileChooser.setFileFilter(new FileFilter() {
		    		 
		    	    public String getDescription() {
		    	        return "Certificate (*.cer)";
		    	    }
		    	 
		    	    public boolean accept(File f) {
		    	        if (f.isDirectory()) {
		    	            return true;
		    	        } else {
		    	            return f.getName().toLowerCase().endsWith(".cer");
		    	        }
		    	    }
		    	});
		    	if (fileChooser.showSaveDialog(CЕRParaKljuceva.this) == JFileChooser.APPROVE_OPTION) 
		    	{
		    		String s= fileChooser.getSelectedFile().getAbsolutePath()+".cer";
		    		s=Utilities.SrediImeFajla(s, ".cer");
		    		fajl=s;
		    		if (s.length()>15)
		    		s= "..."+s.substring(s.length()-15, s.length());
		    		CЕRParaKljuceva.this.chooselabel.setText(s);
		    	}
		    }
		});
		p22.add(choose2);
		p2.add(p22);
		add(p2);
		
		
		JPanel p10= new JPanel();
		p10.setLayout(new GridLayout(1,2));
		JPanel p101= new JPanel();
		JLabel datum1label= new JLabel("Датум почетка важења (дд/ММ/гггг):");
		 
		p101.add(datum1label);
		p10.add(p101);
		JPanel p102= new JPanel();
		datum1=new JTextField();
		datum1.setPreferredSize(new Dimension(100, 20));
		p102.add(datum1);
		p10.add(p102);
		add(p10);
		
		JPanel p11a= new JPanel();
		p11a.setLayout(new GridLayout(1,2));
		JPanel p111= new JPanel();
		JLabel datum2label= new JLabel("Датум истека важења (дд/ММ/гггг):");
		 
		p111.add(datum2label);
		p11a.add(p111);
		JPanel p112= new JPanel();
		datum2=new JTextField();
		datum2.setPreferredSize(new Dimension(100, 20));
		p112.add(datum2);
		p11a.add(p112);
		add(p11a);
		
		
		
		JPanel p12a= new JPanel();
		p12a.setLayout(new GridLayout(1,2));
		JPanel p121= new JPanel();
		JLabel seriallabel= new JLabel("Серијски број кључа:");
		 
		p121.add(seriallabel);
		p12a.add(p121);
		JPanel p122= new JPanel();
		serial=new JTextField();
		serial.setPreferredSize(new Dimension(100, 20));
		p122.add(serial);
		p12a.add(p122);
		add(p12a);
		
		JPanel p13a= new JPanel();
		p13a.setLayout(new GridLayout(1,5));
		JPanel p133= new JPanel();
		bcenable=new JCheckBox("Укључи");
		bcenable.setSelected(false);
		p133.add(bcenable);
		p13a.add(p133);
		JPanel p131= new JPanel();
		JLabel bclabel= new JLabel("Основна ограничења (BC):");
		p131.add(bclabel);
		p13a.add(p131);
		JPanel p13x= new JPanel();
		bcc=new JCheckBox("критична?");
		bcc.setSelected(false);
		p13x.add(bcc);
		p13a.add(p13x);
		
		
		JPanel p132= new JPanel();
		catrue=new JCheckBox("cA?");
		catrue.setSelected(false);
		p132.add(catrue);
		p13a.add(p132);
		JPanel p134= new JPanel();
		bcpathlen=new JTextField();
		bcpathlen.setPreferredSize(new Dimension(100, 20));
		p134.add(bcpathlen);
		p13a.add(p134);
		add(p13a);
		
		
		
		JPanel p14a= new JPanel();
		p14a.setLayout(new GridLayout(1,3));
		JPanel p143= new JPanel();
		ianenable=new JCheckBox("Укључи");
		ianenable.setSelected(false);
		p143.add(ianenable);
		p14a.add(p143);
		
		JPanel p141= new JPanel();
		JLabel ianlabel= new JLabel("Aлтернативна имена издаваоца (IAN):");
		p141.add(ianlabel);
		p14a.add(p141);
		add(p14a);
		
		JPanel p14x= new JPanel();
		ianc=new JCheckBox("критична?");
		ianc.setSelected(false);
		p14x.add(ianc);
		p14a.add(p14x);
		
		
		
		JPanel p15a= new JPanel();
		p15a.setLayout(new GridLayout(1,2));
		JPanel p153= new JPanel();
		String[] options = new String[] {"RFC822Name", "DNSName","directoryName",
                "EDIPartyName", "URIName", "IPAddressName", "RID","X400Name","otherName"};
		ianlist = new JComboBox<>(options);
		p153.add(ianlist);
		//String selectedBook = (String) bookList.getSelectedItem();
		p15a.add(p153);
		JPanel p151= new JPanel();
		ianvalue=new JTextField();
		ianvalue.setPreferredSize(new Dimension(100, 20));
		p151.add(ianvalue);
		p15a.add(p151);
		add(p15a);
		
		JPanel p16a= new JPanel();
		p16a.setLayout(new GridLayout(1,3));
		JPanel p161= new JPanel();
		kuenable=new JCheckBox("Укључи");
		kuenable.setSelected(false);
		p161.add(kuenable);
		p16a.add(p161);
		JPanel p163= new JPanel();
		JLabel kulabel= new JLabel("Коришћење  кључа (KU):");
		p163.add(kulabel);
		p16a.add(p163);
		add(p16a);
		JPanel p16x= new JPanel();
		kuc=new JCheckBox("критична?");
		kuc.setSelected(false);
		p16x.add(kuc);
		p16a.add(p16x);
		
		
		
		JPanel p17a= new JPanel();
		p17a.setLayout(new GridLayout(1,5));
		JPanel p171= new JPanel();
		ku1=new JCheckBox("Digital Signature");
		ku1.setSelected(false);
		p171.add(ku1);
		p17a.add(p171);
		
		JPanel p172= new JPanel();
		ku2=new JCheckBox("Non Repuditarion");
		ku2.setSelected(false);
		p172.add(ku2);
		p17a.add(p172);
		
		JPanel p173= new JPanel();
		ku3=new JCheckBox("Key Enchiperment");
		ku3.setSelected(false);
		p173.add(ku3);
		p17a.add(p173);
		
		JPanel p174= new JPanel();
		ku4=new JCheckBox("Data Enchiperment");
		ku4.setSelected(false);
		p174.add(ku4);
		p17a.add(p174);
		
		JPanel p175= new JPanel();
		ku5=new JCheckBox("Key Agreement");
		ku5.setSelected(false);
		p175.add(ku5);
		p17a.add(p175);
	
		add(p17a);
		
		JPanel p18a= new JPanel();
		p18a.setLayout(new GridLayout(1,4));
		JPanel p181= new JPanel();
		ku6=new JCheckBox("Cerificate Sign");
		ku6.setSelected(false);
		p181.add(ku6);
		p18a.add(p181);
		
		JPanel p182= new JPanel();
		ku7=new JCheckBox("cRL Sign");
		ku7.setSelected(false);
		p182.add(ku7);
		p18a.add(p182);
		
		JPanel p183= new JPanel();
		ku8=new JCheckBox("Encipher Only");
		ku8.setSelected(false);
		p183.add(ku8);
		p18a.add(p183);
		
		JPanel p184= new JPanel();
		ku9=new JCheckBox("Decipher Only");
		ku9.setSelected(false);
		p184.add(ku9);
		p18a.add(p184);
		
	
		add(p18a);
		
		
		
		
		JPanel p0= new JPanel();
		JButton pravi = new JButton("Сачувај");
		pravi.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	
		    		File f = new File(csr);
		    		if(f.exists() && !f.isDirectory()) 
		    		{ 
		    			try
		    			{

		    				PKCS10CertificationRequest request= new PKCS10CertificationRequest(Base64.decode(Files.readAllBytes(Paths.get(csr))));
		    				
		    				RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(request.getSubjectPublicKeyInfo());

		    				RSAPublicKeySpec rsaSpec = new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent());
		    				KeyFactory kf = KeyFactory.getInstance("RSA");
		    				PublicKey publicKey = kf.generatePublic(rsaSpec);
		    				
		    				CЕRParaKljuceva.this.owner.changepotpisujese(publicKey);
		    				
		    				
		    				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    					ParKljuceva par=CЕRParaKljuceva.this.owner.getpotpisujese();
		    				if (par==null) par=new ParKljuceva();
	    					Date vremeod = formatter.parse(datum1.getText());
		    				Date vremedo = formatter.parse(datum2.getText());
		    				if (bcenable.isSelected())
		    				{
		    					if (bcpathlen.getText().length()>0 && (catrue.isSelected()==true))
		    					{
		    						par.setBC(bcc.isSelected(),catrue.isSelected(),Integer.parseInt(bcpathlen.getText()));
		    					}
		    					else
		    					{
		    						par.setBC(bcc.isSelected(),catrue.isSelected(),-1);
		    					}
		    				}
		    				if (kuenable.isSelected())
		    				{
		    					par.setKU(kuc.isSelected(),ku1.isSelected(),ku2.isSelected(),ku3.isSelected(),ku4.isSelected(),ku5.isSelected(),ku6.isSelected(),ku7.isSelected(),ku8.isSelected(),ku9.isSelected());
		    				}
		    				if (ianenable.isSelected())
		    				{
		    					par.setIAN(ianc.isSelected(),ianlist.getSelectedObjects()[0].toString(),ianvalue.getText());
		    					
		    				}
		    				
		    				par.setBasicParameters(vremeod,vremedo,new BigInteger(serial.getText()));
		    				
		    				
		    				try
		    				{
		    					Utilities.saveCER(csr,fajl, CЕRParaKljuceva.this.owner.getPar(),par);
		    					CЕRParaKljuceva.this.owner.refreshpotpisujese();
		    					CЕRParaKljuceva.this.dispose();
		    				}
		    				catch (Exception e1)
		    				{
		    					e1.printStackTrace();
		    					new ErrorDialog(CЕRParaKljuceva.this,"Грешка у чувању фајла!");
		    				}
		    			}
		    			catch(Exception e1)
		    			{
		    				e1.printStackTrace();
		    				new ErrorDialog(CЕRParaKljuceva.this,"Грешка у улазним подацима!");
		    			}
		    			
			    		
		    		}
		    		else
		    		{
		    			new ErrorDialog(CЕRParaKljuceva.this,"Невалидан фајл CSR!");
		    		}
		    	
		    }
		});
		p0.add(pravi);
		add(p0);
		
		setResizable(false);
		setSize(800,600);
		Utilities.centerScreen(this);
		setVisible(true);
	}

}