package projekat;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;

public class UvozParaKljuceva extends JDialog{

	private JPasswordField password;
	private JTextField naziv;
	private JLabel chooselabel;
	private JCheckBox aes;
	private String fajl;
	private Program owner;
	public UvozParaKljuceva(JFrame owner) {
		super(owner,"Увоз пара кључева",true);
		setLayout(new GridLayout(4,1));
		fajl= "podrazumevano.p12";
		this.owner= (Program) owner;
		addWindowListener(
				new WindowAdapter()
				{
					public void windowClosing(WindowEvent a)
					{
						dispose();
					}
				}
				);
		
		JPanel p0= new JPanel();
		p0.setLayout(new GridLayout(1,2));
		JPanel p01= new JPanel();
		JLabel nazivlabel= new JLabel("Назив:");
		p01.add(nazivlabel);
		p0.add(p01);
		JPanel p02= new JPanel();
		naziv=new JTextField();
		naziv.setPreferredSize(new Dimension(100, 20));
		p02.add(naziv);
		p0.add(p02);
		add(p0);
		
		JPanel p1= new JPanel();
		p1.setLayout(new GridLayout(1,3));
		
		JPanel p1x=new JPanel();
		aes=new JCheckBox("шифровати AES-ом?");
		aes.setSelected(false);
		p1x.add(aes);
		p1.add(p1x);
		
		JPanel p11= new JPanel();
		JLabel passwordlabel= new JLabel("Шифра:");
		p11.add(passwordlabel);
		p1.add(p11);
		JPanel p12= new JPanel();
		password=new JPasswordField();
		password.setPreferredSize(new Dimension(100, 20));
		p12.add(password);
		p1.add(p12);
		add(p1);
		
		JPanel p2= new JPanel();
		p2.setLayout(new GridLayout(1,2));
		JPanel p21= new JPanel();
		p21.setLayout(new FlowLayout());
		chooselabel= new JLabel("podrazumevano.p12");
		p21.add(chooselabel);
		p2.add(p21);
		JPanel p22 = new JPanel();
		JButton choose = new JButton("Увези...");
		
		choose.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	JFileChooser fileChooser = new JFileChooser();
		        fileChooser.setAcceptAllFileFilterUsed(false);
		    	fileChooser.setFileFilter(new FileFilter() {
		    		 
		    	    public String getDescription() {
		    	        return "Certificate (*.p12)";
		    	    }
		    	 
		    	    public boolean accept(File f) {
		    	        if (f.isDirectory()) {
		    	            return true;
		    	        } else {
		    	            return f.getName().toLowerCase().endsWith(".p12");
		    	        }
		    	    }
		    	});
		    	if (fileChooser.showOpenDialog(UvozParaKljuceva.this) == JFileChooser.APPROVE_OPTION) 
		    	{
		    		String s= fileChooser.getSelectedFile().getAbsolutePath()+".p12";
		    		s=Utilities.SrediImeFajla(s, ".p12");
		    		fajl=s;
		    		if (s.length()>15)
		    		s= "..."+s.substring(s.length()-15, s.length());
		    		UvozParaKljuceva.this.chooselabel.setText(s);
		    	}
		    }
		});
		p22.add(choose);
		p2.add(p22);
		add(p2);
		
		JPanel p3= new JPanel();
		JButton pravi = new JButton("Увези");
		pravi.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	try{
		    		ParKljuceva par=Utilities.load(fajl,naziv.getText(),password.getPassword(),aes.isSelected());
		    		if (par!=null) 
		    		{
		    			UvozParaKljuceva.this.owner.add(par);
		    			UvozParaKljuceva.this.dispose();
		    		}
		    		else
		    		{
		    			new ErrorDialog(UvozParaKljuceva.this,"Грешка у учитавању фајла!");
		    		}
		    	}
		    	catch (Exception e1)
		    	{
		    		e1.printStackTrace();
		    		new ErrorDialog(UvozParaKljuceva.this,"Грешка у учитавању фајла!");
		    		
		    	}
		    }
		});
		p3.add(pravi);
		add(p3);
		
		setResizable(false);
		setSize(450,200);
		Utilities.centerScreen(this);
		setVisible(true);
	}

}
