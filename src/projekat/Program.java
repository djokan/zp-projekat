package projekat;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.math.BigInteger;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import javax.swing.table.*;

import org.bouncycastle.operator.OperatorCreationException;

import java.security.*;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.LinkedList;
public class Program extends JFrame {
	
	
	private JTable table;
	ParKljuceva potpisujese;
	int selectedpotpisujese;
	public Program() throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException
	{
		super("Пројекат");
		selectedpotpisujese=-1;
		potpisujese=null;
		JMenuBar menubar= new JMenuBar();
		JMenu fajl= new JMenu("Фајл");
		JMenuItem i1 = new JMenuItem("Извоз пара кључева");
		i1.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	if (table.getSelectedRow()==-1)
		    	{
		    		new ErrorDialog(Program.this,"Није изабран ниједан пар кључева!");
		    	}
		    	else
		    	{
		    		new IzvozParaKljuceva(Program.this);
		    	}
		    }
		});
		JMenuItem i2 = new JMenuItem("Увоз пара кључева");
		i2.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		        new UvozParaKljuceva(Program.this);
		    }
		});
		
		
		fajl.add(i1);
		fajl.add(i2);
		menubar.add(fajl);
		setJMenuBar(menubar);
		JPanel p1= new JPanel();
		
		JButton button = new JButton("Генериши пар кључева");
		button.setSize(200,100);
		button.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		       new GenerisiParKljuceva(Program.this);
		    }
		});
		p1.add(button);
		JButton button1 = new JButton("Избриши пар кључева");
		button1.setSize(200,100);
		button1.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	if (table.getSelectedRow()==-1)
		    	{
		    		new ErrorDialog(Program.this,"Није изабран ниједан пар кључева");
		    	}
		    	else
		    	{
		    		((DefaultTableModel) table.getModel()).removeRow(table.getSelectedRow());
		    	}
		    	
		    }
		});
		
		p1.add(button1);
		add(p1,"South");
		
		
		
		
		
		
		addWindowListener(
				new WindowAdapter(){
					public void windowClosing(WindowEvent a){dispose();}
				}
				);
		JPanel p2 = new JPanel(new BorderLayout());
		p2.setBorder(BorderFactory.createTitledBorder("Кључеви"));
		Object data[][]= {{"",""}};
		Object column_names[]= {"Име","Величина","E","CN","OU","O","L","ST","C","Потписан?","BC критичан?","cA?","pathlen","IAN критичан?","IAN тип","IAN вредност","KU критичан?","KU","пар"};
		TableModel model = new DefaultTableModel(data, column_names)
		  {
		    public boolean isCellEditable(int row, int column)
		    {
		      return false;
		    }
		  };
		  table = new JTable( model);
		//kljucevi.setModel(new MyModel());
		  table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		  table.setFillsViewportHeight(true);
		  table.setSize(400, 200);
		//table.getColumnModel().getColumn(0).setPreferredWidth(50);
		//table.getColumnModel().getColumn(1).setPreferredWidth(50);
		//table.getColumnModel().getColumn(2).setPreferredWidth(250);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.removeColumn(table.getColumnModel().getColumn(18));
		((DefaultTableModel) table.getModel()).removeRow(0);
		table.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		        JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.rowAtPoint(p);
		        int column = table.columnAtPoint(p);
		        if (me.getClickCount() == 2) {
		            
		        	
					new TableInfoDialog(Program.this,((DefaultTableModel) table.getModel()).getValueAt(row, column).toString());
					
		        }
		    }
		});
		
		
		JScrollPane scrollPane = new JScrollPane(table);
		p2.add(scrollPane,"Center");
		JPanel p22 = new JPanel();
		p22.setLayout(new GridLayout(1,2));
		JButton button2 = new JButton("CSR пара кључева");
		button2.setSize(200,100);
		button2.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	if (table.getSelectedRow()==-1)
		    	{
		    		new ErrorDialog(Program.this,"Није изабран ниједан пар кључева!");
		    	}
		    	else 
		    	{
		    		new CSRParaKljuceva(Program.this);
		    	}
		    }
		});
		p22.add(button2);
		JButton button3 = new JButton("Потпиши CSR овим паром кључева");
		button3.setSize(200,100);
		button3.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	if (table.getSelectedRow()==-1)
		    	{
		    		new ErrorDialog(Program.this,"Није изабран ниједан пар кључева!");
		    	}
		    	else 
		    	{
		    		new CЕRParaKljuceva(Program.this);
		    	}
		    }
		});
		p22.add(button3);
		p2.add(p22,"South");
		
		
		
		
		add(p2,"Center");
		setResizable(false);
		setSize(1200,500);
		Utilities.centerScreen(this);
		setVisible(true);
		
	}
	
	public ParKljuceva getPar() throws Exception
	{
		 if (table.getSelectedRow()==-1) throw new Exception();
		 return (ParKljuceva) table.getModel().getValueAt(table.getSelectedRow(),18);
		
		
	}
	
	public void changepotpisujese(PublicKey k)
	{
		int i=0;
		BigInteger big;
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		while (i<model.getRowCount())
		{
			big=((RSAPublicKey)(((ParKljuceva) table.getModel().getValueAt(i,18)).getKeys().getPublic())).getModulus();
			
			if (big.equals(((RSAPublicKey)k).getModulus()))
			{
				potpisujese=((ParKljuceva) table.getModel().getValueAt(i,18));
				selectedpotpisujese=i;
				
				break;
			}
			i++;
		};
		if (i==model.getRowCount())
		{
			potpisujese=null;
			selectedpotpisujese=-1;
		}
	}
	
	public void refreshpotpisujese()
	{
		if (potpisujese!=null && selectedpotpisujese!=-1)
		{
			System.out.println("refreshpotpisujeseprosao");
			table.getModel().setValueAt(true, selectedpotpisujese, 9);
			if (potpisujese.isBcenabled())
				table.getModel().setValueAt(potpisujese.isBcc(), selectedpotpisujese, 10);
			if (potpisujese.isBcenabled())
			table.getModel().setValueAt(potpisujese.iscA(), selectedpotpisujese, 11);
			if (potpisujese.isBcenabled())
			table.getModel().setValueAt(potpisujese.getPathlen(), selectedpotpisujese, 12);
			if (potpisujese.isBcenabled())
				table.getModel().setValueAt(potpisujese.isIanc(), selectedpotpisujese, 13);
			if (potpisujese.isIanenabled())
			table.getModel().setValueAt(potpisujese.getIantype(), selectedpotpisujese, 14);
			if (potpisujese.isIanenabled())
			table.getModel().setValueAt(potpisujese.getIanvalue(), selectedpotpisujese, 15);
			if (potpisujese.isBcenabled())
				table.getModel().setValueAt(potpisujese.isKuc(), selectedpotpisujese, 16);
			if (potpisujese.isKuenabled())
			table.getModel().setValueAt(potpisujese.getKu(), selectedpotpisujese, 17);
		}
	}
	
	public ParKljuceva getpotpisujese()
	{
		return potpisujese;
	}
	
	public void add(ParKljuceva k) throws InvalidKeySpecException, NoSuchAlgorithmException
	{
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		
		
		model.addRow(new Object[]{k.getImekljuca(), ((RSAPublicKey)k.getKeys().getPublic()).getModulus().bitLength(), k.getEmail(),k.getCn(), k.getOu(), k.getO(),k.getL(), k.getSt() ,k.getC() , false, null, null, null, null,null, null, null, null, k  });
	}
	
	public static void main(String[] argv) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException
	{
		new Program();
	}
}
