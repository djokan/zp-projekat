package projekat;

import java.awt.FlowLayout;
import java.awt.event.*;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import javax.swing.*;

import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

public class TableInfoDialog extends JDialog{

	public TableInfoDialog(JFrame owner,String s)  {
		super(owner,"Инфо!",true);
		setLayout(new FlowLayout());
		
		JLabel text= new JLabel(s);
		add(text);
		addWindowListener(
				new WindowAdapter(){
					public void windowClosing(WindowEvent a){dispose();}
				}
				);
		
		setResizable(false);
		setSize(600,200);
		Utilities.centerScreen(this);
		setVisible(true);
	}

}
