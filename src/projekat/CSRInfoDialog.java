package projekat;

import java.awt.FlowLayout;
import java.awt.event.*;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import javax.swing.*;

import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

public class CSRInfoDialog extends JDialog{

	public CSRInfoDialog(JDialog owner,String path) throws OperatorCreationException, NoSuchAlgorithmException, InvalidKeySpecException, CertificateException, IOException {
		super(owner,"Инфо!",true);
		setLayout(new FlowLayout());
		PKCS10CertificationRequest req= Utilities.getCSR(path);
		RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(req.getSubjectPublicKeyInfo());

		RSAPublicKeySpec rsaSpec = new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent());
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey publicKey = kf.generatePublic(rsaSpec);
		
		
		JLabel text= new JLabel("<html>Инфо: <br>"+req.getSubject().toString()+"<br>"
				+ "Величина кључа:<br>"+((RSAPublicKey)publicKey).getModulus().bitLength() + ""
						+ "<br>"
						+ "</html>");
		add(text);
		addWindowListener(
				new WindowAdapter(){
					public void windowClosing(WindowEvent a){dispose();}
				}
				);
		
		setResizable(false);
		setSize(600,200);
		Utilities.centerScreen(this);
		setVisible(true);
	}

}
