package projekat;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;

public class CSRParaKljuceva extends JDialog{

	private JLabel chooselabel;
	private String fajl;
	private Program owner;
	public CSRParaKljuceva(JFrame owner) {
		super(owner,"CSR пара кључева",true);
		setLayout(new GridLayout(2,1));
		fajl= "podrazumevano.csr";
		this.owner= (Program) owner;
		addWindowListener(
				new WindowAdapter()
				{
					public void windowClosing(WindowEvent a)
					{
						dispose();
					}
				}
				);
		
		
		
		JPanel p2= new JPanel();
		p2.setLayout(new GridLayout(1,2));
		JPanel p21= new JPanel();
		p21.setLayout(new FlowLayout());
		chooselabel= new JLabel("podrazumevano.csr");
		p21.add(chooselabel);
		p2.add(p21);
		JPanel p22 = new JPanel();
		JButton choose = new JButton("Сачувај као...");
		
		choose.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	JFileChooser fileChooser = new JFileChooser();
		        fileChooser.setAcceptAllFileFilterUsed(false);
		    	fileChooser.setFileFilter(new FileFilter() {
		    		 
		    	    public String getDescription() {
		    	        return "Certificate Signing Request (*.csr)";
		    	    }
		    	 
		    	    public boolean accept(File f) {
		    	        if (f.isDirectory()) {
		    	            return true;
		    	        } else {
		    	            return f.getName().toLowerCase().endsWith(".csr");
		    	        }
		    	    }
		    	});
		    	if (fileChooser.showSaveDialog(CSRParaKljuceva.this) == JFileChooser.APPROVE_OPTION) 
		    	{
		    		String s= fileChooser.getSelectedFile().getAbsolutePath()+".csr";
		    		s=Utilities.SrediImeFajla(s, ".csr");
		    		if(s.length()>15)
		    		s= "..."+s.substring(s.length()-15, s.length());
		    		fajl= fileChooser.getSelectedFile().getAbsolutePath()+".csr";
		    		fajl=Utilities.SrediImeFajla(fajl, ".csr");
		    		
		    		CSRParaKljuceva.this.chooselabel.setText(s);
		    	}
		    }
		});
		p22.add(choose);
		p2.add(p22);
		add(p2);
		
		JPanel p3= new JPanel();
		JButton pravi = new JButton("Сачувај");
		pravi.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	try{
		    		Utilities.saveCSR(CSRParaKljuceva.this.owner.getPar(),fajl);
		    		CSRParaKljuceva.this.dispose();
		    	}
		    	catch (Exception e1)
		    	{
		    		e1.printStackTrace();
		    		new ErrorDialog(CSRParaKljuceva.this,"Грешка у чувању фајла!");
		    		
		    	}
		    }
		});
		p3.add(pravi);
		add(p3);
		
		setResizable(false);
		setSize(300,200);
		Utilities.centerScreen(this);
		setVisible(true);
	}

}
