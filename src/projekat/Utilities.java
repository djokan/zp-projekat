package projekat;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Arrays;
import javax.crypto.*;
import javax.crypto.spec.*;
import javax.swing.*;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.bouncycastle.util.encoders.Base64;

import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.math.BigInteger;
import java.util.Date;

public class Utilities {
	public static boolean isInteger(String str)  
	{  
		  try  
		  {  
		    int d = Integer.parseInt(str);  
		  }
		  catch(NumberFormatException nfe)  
		  {  
		    return false;  
		  }  
		  return true;  
	}
	public static byte[] PrivateKeytoByte(PrivateKey p)
	{
		return p.getEncoded();
		
	}
	
	public static String SrediImeFajla(String fajl,String ext)
	{
		String f=fajl;
		while (f.toLowerCase().endsWith(ext+ext))
		{
			f=f.replaceFirst("(?s)"+ext+"(?!.*?"+ext+")", "");
		}
		return f;
	}
	
	
	
	public static PKCS10CertificationRequest getCSR(String csr) throws OperatorCreationException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, CertificateException
	{
		return new PKCS10CertificationRequest(Base64.decode(Files.readAllBytes(Paths.get(csr))));
	}
	
	
	@SuppressWarnings("deprecation")
	public static void saveCER(String csr,String path,ParKljuceva keys,ParKljuceva arg) throws OperatorCreationException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, CertificateException
	{
		PKCS10CertificationRequest request= new PKCS10CertificationRequest(Base64.decode(Files.readAllBytes(Paths.get(csr))));
		
		RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(request.getSubjectPublicKeyInfo());

		RSAPublicKeySpec rsaSpec = new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent());
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey publicKey = kf.generatePublic(rsaSpec);
		X509v3CertificateBuilder certGen= new JcaX509v3CertificateBuilder(
				keys.getX500Name(),
				arg.getSerial(),
				arg.getVremeod(),
				arg.getVremedo(),
				request.getSubject(),
				publicKey);
		
		
		
		if (arg.isBcenabled())
		{
			if (arg.getPathlen()==-1 || arg.iscA()==false)
			{
				certGen.addExtension(Extension.basicConstraints,arg.isBcc(),new BasicConstraints(arg.iscA()));
			}
			else
			{
				certGen.addExtension(Extension.basicConstraints,arg.isBcc(),new BasicConstraints(arg.getPathlen()));
			}
		}
		if (arg.isIanenabled())
		{
			GeneralName altName=null;
			switch(arg.getIantype())
			{
			case "DNSName":
				altName = new GeneralName(GeneralName.dNSName, arg.getIanvalue());
				break;
			case "RFC822Name":
				altName = new GeneralName(GeneralName.rfc822Name, arg.getIanvalue());
				break;
			case "EDIPartyName":
				altName = new GeneralName(GeneralName.ediPartyName, arg.getIanvalue());
				break;
			case "URIName":
				altName = new GeneralName(GeneralName.uniformResourceIdentifier, arg.getIanvalue());
				break;
			case "IPAddressName":
				altName = new GeneralName(GeneralName.iPAddress, arg.getIanvalue());
				break;
			case "RID":
				altName = new GeneralName(GeneralName.registeredID, arg.getIanvalue());
				break;
			case "X400Name":
				altName = new GeneralName(GeneralName.x400Address, arg.getIanvalue());
				break;
			case "otherName":
				altName = new GeneralName(GeneralName.otherName, arg.getIanvalue());
				break;
			case "directoryName":
				altName = new GeneralName(GeneralName.directoryName, arg.getIanvalue());
				break;
			}
			
			GeneralNames subjectAltName = new GeneralNames(altName);
			certGen.addExtension(X509Extension.issuerAlternativeName,arg.isIanc(),subjectAltName );
		}
		if (arg.isKuenabled())
		{
			KeyUsage usage = new KeyUsage(arg.getKeyusageint());
			certGen.addExtension(Extension.keyUsage, arg.isKuc(), usage); 
			
		}
		
		
		AlgorithmIdentifier sigAlgId = new DefaultSignatureAlgorithmIdentifierFinder().find("SHA1withRSA");
		AlgorithmIdentifier digAlgId = new DefaultDigestAlgorithmIdentifierFinder().find(sigAlgId);
		
		ContentSigner sigGen = new BcRSAContentSignerBuilder(sigAlgId, digAlgId).build(PrivateKeyFactory.createKey(keys.getKeys().getPrivate().getEncoded()) );
		
		CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
		
		InputStream in = new ByteArrayInputStream(certGen.build(sigGen).toASN1Structure().getEncoded());
		X509Certificate cert = (X509Certificate)certFactory.generateCertificate(in);
		
		FileOutputStream f=new FileOutputStream(path);
		//f.write("-----BEGIN CERTIFICATE-----\r\n".getBytes());
		f.write(Base64.encode(cert.getEncoded()));
		//f.write("\r\n-----END CERTIFICATE-----".getBytes());
		f.close();
	}
	
	
	
	public static void saveCSR(ParKljuceva keys,String path) throws OperatorCreationException, IOException
	{
		JcaPKCS10CertificationRequestBuilder builder = 
			     new JcaPKCS10CertificationRequestBuilder(
			         keys.getX500Name(),
			         keys.getKeys().getPublic());
		JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA1withRSA");
		ContentSigner signer = csBuilder.build(keys.getKeys().getPrivate());
		
		PKCS10CertificationRequest request = builder.build(signer);
		
		FileOutputStream f=new FileOutputStream(path);
		//f.write("-----BEGIN CERTIFICATE REQUEST-----\r\n".getBytes());
		f.write(Base64.encode(request.getEncoded()));
		//f.write("\r\n-----END CERTIFICATE REQUEST-----".getBytes());
		f.close();
	}
	
	public static void save(ParKljuceva keys,String path,char[] password,boolean aes) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, OperatorCreationException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException
	{
			KeyStore ks = KeyStore.getInstance("PKCS12");
			ks.load(null, "".toCharArray());
			AlgorithmIdentifier sigAlgId = new DefaultSignatureAlgorithmIdentifierFinder().find("SHA1withRSA");
			AlgorithmIdentifier digAlgId = new DefaultDigestAlgorithmIdentifierFinder().find(sigAlgId);
			
			X509v3CertificateBuilder certGen= new JcaX509v3CertificateBuilder(
					keys.getX500Name(),
					new BigInteger("1"),
					new Date(),
					new Date(),
					keys.getX500Name(),
					keys.getKeys().getPublic());
			
			
			
			
			ContentSigner sigGen = new BcRSAContentSignerBuilder(sigAlgId, digAlgId).build(PrivateKeyFactory.createKey(keys.getKeys().getPrivate().getEncoded()) );
			
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
			
			InputStream in = new ByteArrayInputStream(certGen.build(sigGen).toASN1Structure().getEncoded());
			X509Certificate cert = (X509Certificate)certFactory.generateCertificate(in);
			Certificate[] chain = new Certificate[1];
			chain[0]=cert;
			PrivateKey pk=keys.getKeys().getPrivate();
			//System.out.println(""+new String(((RSAPrivateKey)pk).getModulus().toByteArray()));
			//System.out.println(cert.toString());
			
			//test(keys.getKeys());
			
			ks.setKeyEntry("default", pk,password,chain);
			
			if (aes)
			{
				CipherOutputStream writeStream = Utilities.AesEncryptFile(new FileOutputStream(path),password);
				
				ks.store(writeStream, password);
				writeStream.close();
			}
			else
			{
				OutputStream o = new FileOutputStream(path);
				ks.store( o,password);
				o.close();
			}
			for (int i=0;i<password.length;i++)
			{
				password[i]='0';
			}
		
	}
	static public void test(KeyPair k)
	{
		String s="test";
		try {
		Cipher c1 = Cipher.getInstance("RSA");
		c1.init(Cipher.ENCRYPT_MODE, k.getPrivate());
		Cipher c2 = Cipher.getInstance("RSA");
		c2.init(Cipher.ENCRYPT_MODE, k.getPublic());
		byte[] chipertext=c1.doFinal(s.getBytes());
		System.out.println("private:");
		System.out.println(new String(chipertext));
		byte[] plaintext=c2.doFinal(s.getBytes());
		System.out.println("public:");
		System.out.println(new String(plaintext));
		}catch (Exception e){e.printStackTrace();};
		
	}
	
static public ParKljuceva load(String path,String naziv,char[] password,boolean aes) throws KeyStoreException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, CertificateException, IOException, UnrecoverableKeyException, InvalidKeySpecException
{
	KeyStore ks = KeyStore.getInstance("PKCS12");
	
	if (aes)
	{
		
		CipherInputStream readStream = Utilities.AesDecryptFile( new FileInputStream(path),password);
		ks.load(readStream, password);
		
		readStream.close();
		
	}
	else
	{
		InputStream i=new FileInputStream(path);
		ks.load(i, password);
		i.close();
	}
	
	
	
	
	PrivateKey priv=null;
	
	
	priv= (PrivateKey)ks.getKey(ks.aliases().nextElement(), password);
	
	X509Certificate  cert = (X509Certificate)ks.getCertificate(ks.aliases().nextElement());
	RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(new JcaX509CertificateHolder(cert).getSubjectPublicKeyInfo());

	RSAPublicKeySpec rsaSpec = new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent());
	KeyFactory kf = KeyFactory.getInstance("RSA");
	PublicKey publicKey = kf.generatePublic(rsaSpec);
		
	KeyPair kp= new KeyPair(publicKey, priv);
		    
		    
	X500Name x500name = new JcaX509CertificateHolder(cert).getSubject();
	//test(kp);
	//System.out.println(cert.toString());
	String cn = IETFUtils.valueToString(x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue());
	String email= IETFUtils.valueToString(x500name.getRDNs(BCStyle.E)[0].getFirst().getValue());
	String o= IETFUtils.valueToString(x500name.getRDNs(BCStyle.O)[0].getFirst().getValue());
	String ou= IETFUtils.valueToString(x500name.getRDNs(BCStyle.OU)[0].getFirst().getValue());
	String l= IETFUtils.valueToString(x500name.getRDNs(BCStyle.L)[0].getFirst().getValue());
	String st= IETFUtils.valueToString(x500name.getRDNs(BCStyle.ST)[0].getFirst().getValue());
	String c= IETFUtils.valueToString(x500name.getRDNs(BCStyle.C)[0].getFirst().getValue());
	ParKljuceva par= new ParKljuceva(kp, naziv, email, cn, ou, o, l, st, c);
	for (int i=0;i<password.length;i++)
	{
		password[i]='0';
	}
	return par;

}
	
	
	
	public static boolean isValid(KeyPair k)
	{
		String s="test";
		try {
		Cipher c1 = Cipher.getInstance("RSA");
		c1.init(Cipher.ENCRYPT_MODE, k.getPrivate());
		Cipher c2 = Cipher.getInstance("RSA");
		c2.init(Cipher.DECRYPT_MODE, k.getPublic());
		byte[] chipertext=c1.doFinal(s.getBytes());
		byte[] plaintext=c2.doFinal(chipertext);
		if (new String(plaintext).equals("test")) return true;
		}catch(Exception e) {}
		return false;
	}
	
	public static PrivateKey BytetoPrivateKey(byte[] b) throws InvalidKeySpecException, NoSuchAlgorithmException
	{
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(b);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePrivate(keySpec);
	}
	public static SecretKey StringtoSecretKey(char[] s) throws NoSuchAlgorithmException
	{
		
		byte[] decodedKey = Charset.forName("UTF-8").encode(CharBuffer.wrap(s)).array();
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		decodedKey = sha.digest(decodedKey);
		decodedKey = Arrays.copyOf(decodedKey, 16);
		SecretKey sec=new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
		for (int i=0;i<decodedKey.length;i++)
		decodedKey[i]=0x00;
		return sec;
	}
	
	
	public static byte[] AesEncrypt(byte[] plaintext,char[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
	{	
		Cipher c1 = Cipher.getInstance("AES");
		SecretKey s=StringtoSecretKey(key);
		c1.init(Cipher.ENCRYPT_MODE, s);
		byte[] chipertext=c1.doFinal(plaintext);
		try{
			//s.destroy();
		}catch(Exception e){}
		return chipertext;
	}
	public static CipherOutputStream AesEncryptFile(FileOutputStream file,char[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
	{	
		Cipher c1 = Cipher.getInstance("AES");
		SecretKey s=StringtoSecretKey(key);
		c1.init(Cipher.ENCRYPT_MODE, s);
		return new CipherOutputStream(file, c1);
	}
	public static PrivateKey DecryptPrivateKey(PrivateKey enc,char[] password) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException
	{
		KeyFactory kf = KeyFactory.getInstance("RSA");
		System.out.println(((RSAPrivateKey)enc).getModulus().toByteArray().length);
		return kf.generatePrivate(new RSAPrivateKeySpec(   new BigInteger(Utilities.AesDecrypt(((RSAPrivateKey)enc).getModulus().toByteArray(),password)),((RSAPrivateKey)enc).getPrivateExponent()));
		
		
		
	}
	public static PrivateKey EncryptPrivateKey(PrivateKey p,char[] password) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException
	{
		KeyFactory kf = KeyFactory.getInstance("RSA");
		byte[] cryptedmodulus=Utilities.AesEncrypt(((RSAPrivateKey)p).getModulus().toByteArray(),password);
		System.out.println(cryptedmodulus.length);
		return kf.generatePrivate(new RSAPrivateKeySpec(   new BigInteger(cryptedmodulus),((RSAPrivateKey)p).getPrivateExponent()));
		
	}
	
	public static CipherInputStream AesDecryptFile(FileInputStream file,char[] key) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException
	{
		Cipher c1 = Cipher.getInstance("AES");
		SecretKey s=StringtoSecretKey(key);
		c1.init(Cipher.DECRYPT_MODE, s);
		return new CipherInputStream(file,c1);
	}
	public static byte[] AesDecrypt(byte[] chipertext,char[] key) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException
	{
		Cipher c1 = Cipher.getInstance("AES");
		SecretKey s=StringtoSecretKey(key);
		System.out.println(new String(Arrays.copyOfRange(chipertext,1,chipertext.length)));
		c1.init(Cipher.DECRYPT_MODE, s);
		byte[] plaintext;
		if (chipertext.length%16==1) plaintext=c1.doFinal(Arrays.copyOfRange(chipertext,1,chipertext.length));
		else {
			plaintext=c1.doFinal(chipertext);
			
		}
		try{
			//s.destroy();
		}catch(Exception e){}
		return plaintext;
	}
	
	public static void centerScreen(JFrame a)
	{
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		a.setLocation(dim.width/2-a.getWidth()/2, dim.height/2-a.getHeight()/2);
		
	}
	public static void centerScreen(JDialog a)
	{
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		a.setLocation(dim.width/2-a.getWidth()/2, dim.height/2-a.getHeight()/2);
		
	}
	
	
}
